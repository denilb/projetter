package um.fds.agl.ter23.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import um.fds.agl.ter23.entities.Teacher;
import um.fds.agl.ter23.services.TeacherService;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTestMocked {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private TeacherService teacherService;
    @Captor
    private ArgumentCaptor<Teacher> captor;
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherGet() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("addTeacher"))
                .andReturn();
    }
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        when(teacherService.getTeacher("Kermarrec")).thenReturn(null);
        assumingThat(teacherService.getTeacher("Kermarrec") == null,
                () -> {
                    MvcResult result = mvc.perform(post("/addTeacher")
                                    .param("firstName", "Anne-Marie")
                                    .param("lastName", "Kermarrec")
                                    .param("id", "10")
                            )
                            .andExpect(status().is3xxRedirection())
                            .andReturn();
                });
        verify(teacherService).saveTeacher(captor.capture());
        Teacher added_teacher = captor.getValue();
        assertEquals("Anne-Marie", added_teacher.getFirstName());
        assertEquals("Kermarrec", added_teacher.getLastName());
    }
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception {
        Long existing_id = 10L;
        assumingThat(existing_id != null,
                () -> {
                    MvcResult result = mvc.perform(post("/addTeacher")
                                    .param("firstName", "Anne-Marie")
                                    .param("lastName", "Kermarrec")
                                    .param("id", existing_id.toString())
                            )
                            .andExpect(status().is3xxRedirection())
                            .andReturn();
                });
        verify(teacherService).saveTeacher(captor.capture());
        Teacher added_teacher = captor.getValue();
        assertEquals("Anne-Marie", added_teacher.getFirstName());
        assertEquals("Kermarrec", added_teacher.getLastName());
    }
}