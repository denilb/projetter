package um.fds.agl.ter23.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter23.entities.Sujet;

public interface SujetRepository extends CrudRepository<Sujet, Long> {
    @Override
    //@PreAuthorize("hasRole('ROLE_MANAGER') or (#sujet?.enseignant?.terManager.equals(authentication))")
    @PreAuthorize("hasRole('ROLE_MANAGER') or (hasRole('ROLE_TEACHER') and #sujet?.enseignant?.terManager?.lastName == authentication?.name)")
    Sujet save(@Param("sujet") Sujet sujet);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or (hasRole('ROLE_TEACHER') and #sujet?.enseignant?.terManager?.lastName == authentication?.name)")
    void delete(@Param("sujet") Sujet sujet);
}
