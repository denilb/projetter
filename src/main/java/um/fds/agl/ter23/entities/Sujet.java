package um.fds.agl.ter23.entities;

import javax.persistence.*;

@Entity
public class Sujet {
    private @ManyToOne Teacher enseignant;
    private String titre;

    private @Id    @GeneratedValue Long id;

    public Sujet(){};
    public Sujet(Teacher enseignant, String titre){
        this.enseignant = enseignant;
        this.titre = titre;
    };

    public Teacher getEnseignant(){ return this.enseignant; };
    public void setEnseignant(Teacher enseignant) {
        this.enseignant = enseignant;
    }
    public String getTitre(){ return this.titre; }
    public void setTitre(String titre){ this.titre = titre; }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
    @Override
    public String toString() {
        return "Sujet{" +
                "id=" + getId() +
                ", titre=" + this.titre +
                ", enseignant='" + this.enseignant +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sujet)) return false;
        if (!super.equals(o)) return false;
        Sujet sujet = (Sujet) o;
        return this.enseignant.equals(sujet.getEnseignant()) && this.titre.equals(sujet.getTitre()) && this.id.equals(sujet.getId());
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode();
    }

}
