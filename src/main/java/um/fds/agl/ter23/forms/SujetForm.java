package um.fds.agl.ter23.forms;

public class SujetForm {
    private String titreSujet;
    private long id;

    public SujetForm(long id, String titreSujet) {
        this.titreSujet = titreSujet;
        this.id = id;
    }

    public SujetForm() {}
    public String getTitreSujet() {
        return titreSujet;
    }

    public void setTitreSujet(String titreSujet) {
        this.titreSujet = titreSujet;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
