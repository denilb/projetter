package um.fds.agl.ter23.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter23.entities.Sujet;
import um.fds.agl.ter23.forms.SujetForm;
import um.fds.agl.ter23.services.SujetService;
import um.fds.agl.ter23.services.TeacherService;

@Controller
public class SujetController {
    @Autowired
    private SujetService sujetService;
    @Autowired
    private TeacherService teacherService;
    @GetMapping("/listSujets")
    public Iterable<Sujet> getSujets(Model model) {
        Iterable<Sujet> sujets=sujetService.getSujets();
        model.addAttribute("sujets", sujets);
        return sujets;
    }
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = { "/addSujet" })
    public String showAddSujetPage(Model model) {

        SujetForm sujetForm = new SujetForm();
        model.addAttribute("sujetForm", sujetForm);

        return "addSujet";
    }
    @PostMapping(value = { "/addSujet"})
    public String addSujet(Model model, @ModelAttribute("SujetForm") SujetForm sujetForm) {
        Sujet s;
        if(sujetService.findById(sujetForm.getId()).isPresent()){
            // sujet already existing : update
            s = sujetService.findById(sujetForm.getId()).get();
            s.setTitre(sujetForm.getTitreSujet());
        } else {
            // sujet not existing : create
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            s=new Sujet(teacherService.getTeacher(auth.getName()), sujetForm.getTitreSujet());
        }
        sujetService.saveSujet(s);
        return "redirect:/listSujets";
    }

    @GetMapping(value = {"/showSujetUpdateForm/{id}"})
    public String showSujetUpdateForm(Model model, @PathVariable(value = "id") long id){

        SujetForm sujetForm = new SujetForm(id, sujetService.findById(id).get().getTitre());
        model.addAttribute("sujetForm", sujetForm);
        return "updateSujet";
    }

    @GetMapping(value = {"/deleteSujet/{id}"})
    public String deleteSujet(Model model, @PathVariable(value = "id") long id){
        sujetService.deleteSujet(id);
        return "redirect:/listSujets";
    }
}
